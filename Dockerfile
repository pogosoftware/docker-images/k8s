FROM alpine:3.13.2

ARG HELM3_VERSION=3.5.2
ARG KUBECTL_VERSION=1.20.4
ARG HELMFILE_VERSION=0.138.4

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

RUN adduser -D -S -H kubernetes

# Install helm 3
ENV BASE_URL="https://get.helm.sh"
ENV HELM3_TAR_FILE="helm-v${HELM3_VERSION}-linux-amd64.tar.gz"
RUN apk add --update --no-cache curl ca-certificates bash git && \
    curl -L ${BASE_URL}/${HELM3_TAR_FILE} |tar xvz && \
    mv linux-amd64/helm /usr/bin/helm && \
    chmod +x /usr/bin/helm && \
    rm -rf linux-amd64 && \
    rm -f /var/cache/apk/*

# add helm-diff
RUN /usr/bin/helm plugin install https://github.com/databus23/helm-diff

# add helmfile
RUN curl -L https://github.com/roboll/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_linux_amd64 --output /usr/bin/helmfile && \
    chmod +x /usr/bin/helmfile

# Install kubectl
RUN curl -L https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl --output /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl

# Install awscli
RUN apk add --update --no-cache python3 && \
    python3 -m ensurepip && \
    pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir awscli && \
    rm -f /var/cache/apk/*

# Install jq
RUN apk add --update --no-cache jq && \
    rm -f /var/cache/apk/*

ENTRYPOINT ["bash"]