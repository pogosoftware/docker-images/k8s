# Kubernetes docker image

This image have installed applications:
* Helm 3 (3.5.2)
* helm diff plugin
* Helmfile (0.138.4)
* Kubectl (1.20.4)
* aws cli

## Helm 3
To using Helm 3 use command
```bash
/usr/bin/helm
```